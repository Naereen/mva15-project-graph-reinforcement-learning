# Idée : reboucler sur Pengkun

## Idée 1
- pour m = 1 .. M
  - on apprend Qm la décision de l'expert m (joueur X), on a pi_m (via LSTD-Q)
  - on apprend rho/Q sur l'adversaire m (joueur O)
  - on a appris la MDP de l'adversaire de m
  - on fait Pengkun : T roll-out face à cette MDP_m
  - ca donne un decision process pi_{Pengkun, m} (pas fonctionnel mais pour un state)

## Idée 2
- pour m = 1 .. M
  - on apprend Qm la décision de l'adversaire A_m (joueur O), on a pi_m (via LSTD-Q)
  - on fait Pengkun : T roll-out face à cet adversaire,
  - ca donne un decision process pi_{Pengkun, m} (pas fonctionnel mais pour un state)


Un Pengkun pour chaque expert m pour X -> apprendre pi_{Pengkun, X, m} -> policy
Un Pengkun pour chaque expert m pour O -> apprendre pi_{Pengkun, O, m} -> indice de performance de l'expert m


Comment combiner pi_m et pi_{Pengkun, m} ?

Comment combiner pi_{Pengkun, m} ?
- Chacune a une distribution pour ses rewards (Gaussienne de centre reward_m, variance 1)
- Ca donne un poids reward_m, renormaliser ~= performance e(m)

On s'en sert pour calculer E_m(pi_m) = sum_m e(m) \pi_m

---

## Idée 3
a. Training
- pour m = 1 .. M
  - on a Dm = {d_m^{(i)}} une collection de demonstrations
  - on apprend Qm la policy de l'adversaire A_m (joueur O), on a pi_m^{(a)} (via LSTD-Q)

b. Playing
- pour jouer à l'instant t :
  - pm = proba de se trouver face à l'adversaire A_m
  - pm = pi_m^{(a)}(a'_t, s'_t) (proba historique) = 
  - on fait Pengkun : T roll-out face à cet adversaire,
  - ca donne un decision process pi_{Pengkun, m} (pas fonctionnel mais pour un state)


---

# Idée :
Toussou13

- pour k = 1 .. K
  - sampler beta^{(k)} selon un prior (ou hyper-prior)
  - apprendre w_{m,Q}^{(k)} pour chaque expert avec LSTD-Q, ou LSTD-Q contraint (|w|=1)
  - les beta_m donne une évaluation de la température de chaque joueur == confidence.

----

# Combiner les policies

4 idées pour combiner les pi_m :

1. un prior knowledge, fixée, on le croit (e.g. ELO),
2. on fait K=100 parties face à pi_0 (soit random, soit un quelconque joueur), on regarder qui gagne le plus, on normalise -> e(m), on combine (linéairement) -> pi*
3. apprendre w_Q^{(m)} pour chaque expert, normaliser dans [-1,1] -> une température beta_m -> on normalise -> e(m), on combine (linéairement) -> pi*,
4. utiliser Pengkun en temps réel dans le jeu : à chaque instant t (a chaque coup), pour chaque m on fait un Pengkun (face au pi_m du joueur X), qui donne un score (au bout de T parties), et we get a distribution on players for ONE decision prise de décision à l'instant t


- L'idée 1. est foireuse mais pour ELO peut marcher ?
- L'idée 2. : Monte-Carlo trials = peut marcher (comparaison face à un adversaire fixe). Moins bien que 4.,
- L'idée 3. : foireux, tout court, mais à décrire quand même (lien beta <-> varepsilon du exploration-exploitation),
- L'idée 4. : super malin ! A écrire proprement



## Remarque : HUGE
- Symétrie du jeu : très important !
- Jeu à deux joeurs, symétrique !
- On met 0 si draw, pas -1 (because it has to be symmetric)
