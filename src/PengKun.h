// Most of the following is taken from Pengkun's code
// https://github.com/Charles-Lau-/thesis/tree/master/code

#include "NormalDistribution.h"

real GetVariance(const NormalDistribution& dist)
{
  return dist.s * dist.s;
}

NormalDistribution Gaussian(real mean, real var)
{
  return NormalDistribution(mean, sqrt(var));
}

NormalDistribution MultiplyGaussian(const NormalDistribution& lhs, const NormalDistribution& rhs)
{
  if (GetVariance(lhs) == 0)
    return rhs;

  if (GetVariance(rhs) == 0)
    return lhs;

  double newVariance = 1 / (1 / GetVariance(lhs) + 1 / GetVariance(rhs));
  double newMean = (lhs.getMean() / GetVariance(lhs) + rhs.getMean() / GetVariance(rhs)) * newVariance;

  return Gaussian(newMean, newVariance);
}

NormalDistribution AddGaussian(const NormalDistribution& lhs, const NormalDistribution& rhs)
{
  return Gaussian(lhs.getMean() + rhs.getMean(), GetVariance(lhs) + GetVariance(rhs));
}

NormalDistribution SubGaussian(const NormalDistribution& lhs, const NormalDistribution& rhs)
{
  return Gaussian(lhs.getMean() - rhs.getMean(), GetVariance(lhs) - GetVariance(rhs));
}

NormalDistribution DivideGaussian(const NormalDistribution& lhs, const NormalDistribution& rhs)
{
  if (GetVariance(rhs) == 0)
    return lhs;

  if (GetVariance(lhs) == GetVariance(rhs))
    return Gaussian(0, 0);

  double newVariance = 1 / (1 / GetVariance(lhs) - 1 / GetVariance(rhs));
  double newMean = (lhs.getMean() / GetVariance(lhs) - rhs.getMean() / GetVariance(rhs)) * newVariance;

  return Gaussian(newMean, newVariance);
}

real GaussianPDF(real x)
{
	real a = x*x;
	real b = -a/2;
	real c = exp(b);
	real coefficient = pow(M_PI*2,-0.5);

	return coefficient*c;
}

real GaussianPhi(real x)
{
	real a1 =  0.254829592;
	real a2 = -0.284496736;
    	real a3 =  1.421413741;
    	real a4 = -1.453152027;
    	real a5 =  1.061405429;
    	real p  =  0.3275911;

    	// Save the sign of x
    	int sign = 1;
    	if(x < 0)
        	sign = -1;
    	x = fabs(x)/sqrt(2.0);

 	// A&S formula 7.1.26
    	real t = 1.0/(1.0 + p*x);
    	real y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    	return 0.5*(1.0 + sign*y);


}

class Node {
public:
  Node(Node* parent, TicTacToe&& game, size_t action)
    : m_parent(parent)
    , m_game(std::move(game))
    , m_action(action)
  {
  }

  void SetGDis(NormalDistribution normal)
  {
    m_gdis = normal;
  }

  NormalDistribution GDis() const { return m_gdis; }

  void Visit()
  {
    m_visited = true;
  }

  void PopulateChildren()
  {
    if (m_game.Ended())
      return;

    if (m_children.size() > 0)
      return;

    m_game.ForEachMove([&] (size_t ix, TicTacToe&& next) {
        m_children.push_back(Node(this, std::move(next), ix));
      });
  }

  Node& GetAChild()
  {
    PopulateChildren();

    assert(m_children.size() > 0);

    return m_children[MersenneTwister::random() % m_children.size()];
  }

  size_t BestAction()
  {
    assert(!m_game.Ended() && m_children.size() > 0);

    double bestG = 0;
    bool did = false;
    size_t bestAction;
    for (const Node& child : m_children) {
      double g = child.m_gdis.generate();

      if (!did || g > bestG) {
        did = true;
        bestG = g;
        bestAction = child.m_action;
      }
    }

    return bestAction;
  }

  template<typename WhitePlayer, typename BlackPlayer>
  NormalDistribution Descent(Player realPlayer, const WhitePlayer& whitePlayer, const BlackPlayer& blackPlayer)
  {
    if (m_visited && !m_game.Ended()) {
      Node& child = GetAChild();

      child.UpdateMessageFromParent();
      child.UpdateMessageToParent(child.Descent(realPlayer, whitePlayer, blackPlayer));
    } else {
      UpdateMessageExceptRollOut();

      m_rollout = RollOutMessage(realPlayer, whitePlayer, blackPlayer);

      if (m_game.Ended())
        m_gdis = m_rollout;
      else
        m_gdis = MultiplyGaussian(m_gdis, m_rollout);
    }

    auto messageExceptP = DivideGaussian(m_gdis, m_messageFromParent);
    messageExceptP.setVariance(GetVariance(messageExceptP) + 1);
    return messageExceptP;
  }

  template<typename WhitePlayer, typename BlackPlayer>
  NormalDistribution RollOutMessage(Player realPlayer, const WhitePlayer& whitePlayer, const BlackPlayer& blackPlayer)
  {
    TicTacToe game = m_game;

    size_t length = 0;

    while (!game.Ended()) {
      if (game.NextPlayer() == Player::White)
        whitePlayer(game);
      else
        blackPlayer(game);
      ++length;
    }

    int result = -1;

    /*
      if (game.Winner() == Player::Blank)
      result = 0;
    */

    if (game.Winner() == realPlayer)
      result = 1;

    NormalDistribution prior = Gaussian(m_gdis.getMean(), GetVariance(m_gdis) + length);

    real k = prior.m / prior.s;
    real p, firstMoment;
    if (result > 0) {
      // First moment
      real n = GaussianPDF(k);
      real d = GaussianPhi(k);
      // Second moment
      p = (1 + k * n / d);
      firstMoment = prior.m + prior.s * (n / d);
    } else if (result < 0) {
      // First moment
      real n = GaussianPDF(k);
      real d = GaussianPhi(-k);
      // Second moment
      p = (1 - k * n / d);
      firstMoment = prior.m - prior.s * (n / d);
    } else
      return Gaussian(0, 0);
    real secondMoment = prior.m * prior.m + GetVariance(prior) * p;

    return Gaussian(firstMoment, secondMoment - firstMoment * firstMoment + length);
  }

  void UpdateMessageFromParent()
  {
    m_gdis = DivideGaussian(m_gdis, m_messageFromParent);
    auto messageExceptC = DivideGaussian(m_parent->m_gdis, m_messageToParent);
    m_messageFromParent = Gaussian(messageExceptC.getMean(), GetVariance(messageExceptC) + 1);
    m_gdis = MultiplyGaussian(m_gdis, m_messageFromParent);
  }

  void UpdateMessageExceptRollOut()
  {
    NormalDistribution newParent = DivideGaussian(m_parent->m_gdis, m_parent->m_rollout);
    m_gdis = DivideGaussian(m_gdis, m_messageFromParent);
    NormalDistribution messageExceptC = DivideGaussian(newParent, m_messageToParent);
    m_messageFromParent = Gaussian(messageExceptC.getMean(), GetVariance(messageExceptC) + 1);
    m_gdis = MultiplyGaussian(m_gdis, m_messageFromParent);
  }

  void UpdateMessageToParent(const NormalDistribution& message)
  {
    m_parent->m_gdis = DivideGaussian(m_parent->m_gdis, m_messageToParent);
    m_messageToParent = message;
    m_parent->m_gdis = MultiplyGaussian(m_parent->m_gdis, m_messageToParent);
  }

private:
  NormalDistribution m_gdis = Gaussian(0, 0);
  bool m_visited = false;
  Node* m_parent;
  TicTacToe m_game;
  std::vector<Node> m_children { };
  size_t m_action;
  NormalDistribution m_messageFromParent = Gaussian(0, 0);
  NormalDistribution m_messageToParent = Gaussian(0, 0);
  NormalDistribution m_rollout = Gaussian(0, 0);
};
