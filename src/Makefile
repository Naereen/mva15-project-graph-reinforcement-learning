# Makefile for a small C++ program
# (C) Basile Clement and Lilian Besson, 2015
# https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
# See the README.md file for dependances and explanation for installing and building
CC = clang++
CXX = clang++

ifeq ($(OS),Windows_NT)
    CCFLAGS += -D WIN32
    ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
        CCFLAGS += -D AMD64
    endif
    ifeq ($(PROCESSOR_ARCHITECTURE),x86)
        CCFLAGS += -D IA32
    endif
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        CCFLAGS += -D LINUX
		CC = g++
		CXX = g++
    endif
    ifeq ($(UNAME_S),Darwin)
        CCFLAGS += -D OSX
		CC = clang++
		CXX = clang++
    endif
    UNAME_P := $(shell uname -p)
    ifeq ($(UNAME_P),x86_64)
        CCFLAGS += -D AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        CCFLAGS += -D IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        CCFLAGS += -D ARM
    endif
endif

CCFLAGS = -std=c++11 -O3 -Wall -Wextra -DUSE_DOUBLE
CXXFLAGS = $(CCFLAGS) -I../beliefbox/src/algorithms -I../beliefbox/src/environments -I../beliefbox/src/models -I../beliefbox/src/core -I../beliefbox/src/statistics -I/Users/elarnon/local/include

BELIEFBOX_DIR := ../beliefbox/src
_BELIEFBOX_OBJS := Vector.o Object.o
BELIEFBOX_OBJS := $(patsubst %,$(BELIEFBOX_DIR)/objs/%,$(_BELIEFBOX_OBJS))

all: main

run: main
	LD_LIBRARY_PATH=$(BELIEFBOX_DIR)/src/export/lib ./main

main: main.o TicTacToe.o
	$(CXX) $(CXXFLAGS) $^ -L$(BELIEFBOX_DIR)/lib -lsmpl -L$(BELIEFBOX_DIR)/export/lib -lranlib -lgsl -lcblas -o $@

%.o: %.cc
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -rf *.o main
