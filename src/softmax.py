#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Just a very small Python script to compute a soft-max.

It shows how we can easily get overflow errors when computing soft-max, even for small games.
It demonstrates the use of a logsumexp function to compute log-likelihoods.

- *Date:* Friday 08 January 2016, 21:45:00.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
import numpy as np

# log trick (to reduce underflow risks)
try:
    from scipy.misc import logsumexp
except ImportError:
    def logsumexp(li):
        """ Our own log-sum-exp function (**naive**, not optimized)."""
        return np.log(np.sum(np.exp(li)))
    print("We defined our own logsumexp function, scipy.misc.logsumexp should be use instead!")


beta = 1


def softmax(Qm, beta=beta):
    """ Softmax with temperature = beta."""
    # def f(s):
    #     return np.exp(beta * Qm[s, :]) / np.sum(np.exp(beta * Qm))
    # return f
    return np.exp(beta * Qm) / np.sum(np.exp(beta * Qm))


def normalized_softmax(Qm, beta=beta):
    """ Softmax with temperature = beta."""
    betaQ = np.max(np.abs(Qm))
    beta = beta * betaQ
    Qm_normalized = Qm / betaQ
    # def f(s):
    #     return np.exp(beta * Qm_normalized[s, :]) / np.sum(np.exp(beta * Qm_normalized))
    # return f
    return np.exp(beta * Qm_normalized) / np.sum(np.exp(beta * Qm_normalized))


def log_softmax(Qm, beta=beta):
    """ Log Softmax with temperature = beta."""
    # def f(s):
    #     return np.exp(beta * Qm[s, :]) / np.sum(np.exp(beta * Qm))
    # return f
    return beta * Qm - logsumexp(beta * Qm)


def test(Q, beta1, beta2):
    """ Test function. """
    print(" - First expert: beta1 =", beta1)
    print("    Q_1 =", Q[0, :])
    pi_1 = softmax(Q[0, :], beta=beta1)
    print("    By softmax, pi_1 =", pi_1)
    llh_1 = log_softmax(Q[0, :], beta=beta1)
    print("    By softmax, llh_1 =", llh_1)
    print("- Second expert: beta1 =", beta2)
    print("    Q_2 =", Q[1, :])
    pi_2 = softmax(Q[1, :], beta=beta2)
    print("    By softmax, pi_2 =", pi_2)
    llh_2 = log_softmax(Q[1, :], beta=beta2)
    print("    By softmax, llh_2 =", llh_2)
    # Play !
    print("\nOn this state s, they chose:")
    print("- First expert:", 1 + np.argmax(pi_1))
    print("- Second expert:", 1 + np.argmax(pi_2))
    return np.array([pi_1, pi_2])


if __name__ == '__main__':
    print("Example of computation of soft-max pi_m(a|s) from beta and Q_m(s,a).")
    A = 2  # Number of actions
    M = 2  # Number of experts
    # A × M value function Q.
    print("First example, big values for Q:")
    Q = np.array([
                  [800, 300],
                  [1, 9]
        ])
    print("\n# For beta1 = 1, beta2 = 100")
    test(Q, 1, 100)
    print("\n# For beta1 = 100, beta2 = 1")
    test(Q, 100, 1)
    print("\n# For beta1 = 0.01, beta2 = 0.01")
    test(Q, 0.01, 0.01)
    print("\n# For beta1 = 0.00001, beta2 = 0.00001")
    test(Q, 0.00001, 0.00001)
    # A × M value function Q.
    print("First example, homogenuous values for Q:")
    Q = np.array([
                  [0.8, 0.3],
                  [0.1, 0.9]
        ])
    print("\n# For beta1 = 1, beta2 = 1")
    pi_1, pi_2 = test(Q, 1, 1)
    e = np.array([0.80, 0.20])
    print("Distribution on the players : e =", e)
    pistar = e[0]*pi_1 + e[1]*pi_2
    print("Aggregated policy pi* by expectation on the pi =", pistar)
    Qstar = e[0]*Q[0, :] + e[1] * Q[1, :]
    betastar = 1
    pistar2 = softmax(Qstar, beta=betastar)
    print("Aggregated policy pi* by expectation on the Q =", pistar2)
    # Done !

# End of softmax.py
