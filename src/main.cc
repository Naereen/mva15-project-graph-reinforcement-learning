// (C) Basile Clement and Lilian Besson, 2015
// https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
#include "TicTacToe.h"

#include <unordered_set>
#include <unordered_map>

#include "MersenneTwister.h"

#include <getopt.h>

// beliefbox (from https://github.com/olethrosdc/beliefbox)
#include "../beliefbox/src/core/ReadFile.h"

size_t minmax(const TicTacToe& x) {
  // printf("Minimax for player %c\n", x.NextPlayer() == Player::White ? 'X' : 'O');
  std::unordered_map<TicTacToe, int> values;
  std::unordered_set<TicTacToe> visited;
  std::vector<TicTacToe> queue { x };
  while (!queue.empty()) {
    auto game = queue.back();
    if (visited.find(game) == visited.end()) {
      game.ForEachMove([&] (size_t, TicTacToe&& next) {
	  if (visited.find(next) == visited.end())
	    queue.push_back(std::move(next));
	});
      visited.insert(game);
    } else {
      queue.pop_back();
      if (game.NumberOfMoves() == 0) {
	if (game.Winner() == x.NextPlayer())
	  values[game] = 1;
	else if (game.Winner() == Player::Blank)
	  values[game] = 0;
	else
	  values[game] = -1;
      } else {
	assert(values.find(game.WithMove(0)) != values.end());
	int value = values[game.WithMove(0)];
	for (size_t i = 1; i < game.NumberOfMoves(); ++i) {
	  assert(values.find(game.WithMove(i)) != values.end());
	  int v = values[game.WithMove(i)];
	  if (game.NextPlayer() == x.NextPlayer() && v > value)
	    value = v;
	  if (game.NextPlayer() != x.NextPlayer() && v < value)
	    value = v;
	}
	values[game] = value;
      }
    }
  }

  int value = values[x.WithMove(0)];
  size_t best = 0;
  for (size_t i = 1; i < x.NumberOfMoves(); ++i) {
    int v = values[x.WithMove(i)];
    if (v > value) {
      value = v;
      best = i;
    }
  }
  // printf("Best value %d\n", value);
  return best;
}

void IntVectorToFile(const std ::vector<int>& data, const char* fname);

// beliefbox (from https://github.com/olethrosdc/beliefbox)
#include "../beliefbox/src/algorithms/Demonstrations.h"

void AddDemonstration(Demonstrations<TicTacToe, std::pair<size_t, size_t>>& demonstrations,
		      Player me, size_t size, const std::vector<std::pair<size_t, size_t>>& history)
{
  TicTacToe game(size);
  demonstrations.NewEpisode();
  for (auto pair : history) {
    if (game.NextPlayer() == me)
      demonstrations.Observe(game, pair);
    game.Play(pair.first, pair.second);
  }
  if (!game.Ended())
    throw std::runtime_error("history is not complete.");
  demonstrations.Terminate();
}

struct log_likelihood_state {
  size_t n_demo;
  gsl_vector* factor;
  std::vector<std::pair<size_t, std::vector<size_t>>> groups;
  std::vector<gsl_vector*> all_features;
};

/*
  void PrepareFunction(const Expert)
  {
  log_likelihood_state state { };

  std::unordered_map<TicTacToe, size_t> feature_cache;
  std::unordered_map<TicTacToe, size_t> group_cache;

  for (uint d = 0; d < demonstrations.size(); ++d) {
  for (uint t = 0; t < demonstrations.length(i); ++t) {
  TicTacToe game = demonstrations.state(d, t);
  Vector features = game.WithPlay(demonstrations.action(d, t)).features();
  if (state->factor.Size() == 0)
  state->factor = std::move(features);
  else
  state->factor += features;

  auto it = group_cache.find(game);
  if (it == group_cache.end()) {
  std::vector<size_t> current;
  demonstrations.state(d, t).ForEachMove([&] (size_t, TicTacToe&& next) {
  auto it = feature_cache.find(next);
  size_t feature_idx;
  if (*it == feature_cache.end()) {
  current.push_back(state->all_features.size());
  feature_cache.insert({ next, state->all_features.size() });
  state->all_features.push_back(next.features());
  } else
  current.push_back(it->second);
  });

  group_cache.insert({ game, state->groups.size() });
  state->groups.push_back({ 1, std::move(current) });
  } else
  state->groups[it->second].first++;
  }
  }
  }*/


typedef Demonstrations<TicTacToe, std::pair<size_t, size_t>> Expert;

// GSL (from https://www.gnu.org/software/gsl/)
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sf.h>

double compute_log_likelihood(const gsl_vector* w, void* params)
{
  log_likelihood_state* state = static_cast<log_likelihood_state*>(params);

  double result = 0;
  gsl_blas_ddot(w, state->factor, &result);

  for (const auto& group : state->groups) {
    double sum = 0;
    for (const auto& feature_idx : group.second) {
      double scalar = 0;
      gsl_blas_ddot(w, state->all_features[feature_idx], &scalar);
      sum += gsl_sf_exp(scalar);
    }
    result -= group.first * gsl_sf_log(sum);
  }

  // We actually want to maximize
  return -result / state->n_demo;
}

void compute_log_likelihood_grad(const gsl_vector* w, void* params, gsl_vector* g)
{
  log_likelihood_state* state = static_cast<log_likelihood_state*>(params);

  gsl_vector* coeff = gsl_vector_alloc(w->size);
  gsl_vector* feature = gsl_vector_alloc(w->size);

  gsl_vector_memcpy(g, state->factor);

  for (const auto& group : state->groups) {
    for (size_t i = 0; i < coeff->size; ++i)
      gsl_vector_set(coeff, i, 0);

    double sum = 0;

    for (const auto& feature_idx : group.second) {
      gsl_vector_memcpy(feature, state->all_features[feature_idx]);
      double scalar;
      gsl_blas_ddot(w, feature, &scalar);
      double exp = gsl_sf_exp(scalar);
      sum += exp;
      gsl_vector_scale(feature, exp);
      gsl_vector_add(coeff, feature);
    }

    gsl_vector_scale(coeff, group.first / sum);
    gsl_vector_sub(g, coeff);
  }

  gsl_vector_free(coeff);
  gsl_vector_free(feature);

  // We actually want to maximize
  gsl_vector_scale(g, -1 / real(state->n_demo));
}

void compute_log_likelihood_and_grad(const gsl_vector *w, void *params, double *f, gsl_vector *g)
{
  log_likelihood_state* state = static_cast<log_likelihood_state*>(params);

  gsl_vector* coeff = gsl_vector_alloc(w->size);
  gsl_vector* feature = gsl_vector_alloc(w->size);

  gsl_vector_memcpy(g, state->factor);
  *f = 0;

  for (const auto& group : state->groups) {
    for (size_t i = 0; i < coeff->size; ++i)
      gsl_vector_set(coeff, i, 0);

    double sum = 0;

    for (const auto& feature_idx : group.second) {
      gsl_vector_memcpy(feature, state->all_features[feature_idx]);
      double scalar;
      gsl_blas_ddot(w, feature, &scalar);
      double exp = gsl_sf_exp(scalar);
      sum += exp;
      gsl_vector_scale(feature, exp);
      gsl_vector_add(coeff, feature);
    }

    gsl_vector_scale(coeff, group.first / sum);
    gsl_vector_sub(g, coeff);

    *f -= group.first * gsl_sf_log(sum);
  }

  gsl_vector_free(coeff);
  gsl_vector_free(feature);

  // We actually want to maximize, not minimize!
  *f = -*f / state->n_demo;
  gsl_vector_scale(g, -1 / real(state->n_demo));
}

// GSL (from https://www.gnu.org/software/gsl/)
#include <gsl/gsl_multimin.h>

Vector FitExpert(const Expert& demonstrations)
{
  log_likelihood_state state { };

  state.n_demo = demonstrations.size();

  assert(state.factor == nullptr);

  std::unordered_map<TicTacToe, size_t> feature_cache;
  std::unordered_map<TicTacToe, size_t> group_cache;

  gsl_vector* feature = nullptr;

  for (uint d = 0; d < demonstrations.size(); ++d) {
    for (uint t = 0; t < demonstrations.length(d); ++t) {

      TicTacToe game = demonstrations.state(d, t);
      Vector features = game.WithPlay(demonstrations.action(d, t)).features();

      if (!feature)
	feature = gsl_vector_alloc(features.Size());
      for (size_t i = 0; i < feature->size; ++i)
	gsl_vector_set(feature, i, features[i]);

      if (!state.factor)
	state.factor = gsl_vector_calloc(features.Size());

      gsl_vector_add(state.factor, feature);

      auto it = group_cache.find(game);
      if (it == group_cache.end()) {
	std::vector<size_t> current;
	demonstrations.state(d, t).ForEachMove([&] (size_t, const TicTacToe& next) {
            // assert(next.Size() == 3);
	    auto it = feature_cache.find(next);
	    if (it == feature_cache.end()) {
	      current.push_back(state.all_features.size());
	      feature_cache.insert({ next, state.all_features.size() });

	      Vector next_features = next.features();
              // assert(next_features.Size() == 10);
	      gsl_vector* state_feature = gsl_vector_alloc(next_features.Size());
	      for (size_t i =0; i < state_feature->size; ++i)
		gsl_vector_set(state_feature, i, next_features[i]);
	      state.all_features.push_back(state_feature);
	    } else
	      current.push_back(it->second);
	  });

	group_cache.insert({ game, state.groups.size() });
	state.groups.push_back({ 1, std::move(current) });
      } else
	state.groups[it->second].first++;
    }
  }

  assert(feature);
  size_t size = feature->size;
  gsl_vector_free(feature);

  gsl_multimin_function_fdf log_likelihood;

  log_likelihood.n = size;
  log_likelihood.f = &compute_log_likelihood;
  log_likelihood.df = &compute_log_likelihood_grad;
  log_likelihood.fdf = &compute_log_likelihood_and_grad;
  log_likelihood.params = static_cast<void*>(&state);

  const gsl_multimin_fdfminimizer_type* minimizer_type = gsl_multimin_fdfminimizer_vector_bfgs2;

  gsl_multimin_fdfminimizer* minimizer = gsl_multimin_fdfminimizer_alloc(minimizer_type, size);

  gsl_vector* zero = gsl_vector_calloc(size);

  gsl_multimin_fdfminimizer_set(minimizer, &log_likelihood, zero, 0.01, 1e-4);

  size_t iter = 0;
  int status;
  do {
    ++iter;
    status = gsl_multimin_fdfminimizer_iterate(minimizer);

    if (status)
      break;

    status = gsl_multimin_test_gradient(minimizer->gradient, 1e-2);
  } while (status == GSL_CONTINUE && iter < 100);

  Vector weights(static_cast<uint>(size));

  for (size_t i = 0; i < size; ++i)
    weights[i] = gsl_vector_get(minimizer->x, i);

  gsl_multimin_fdfminimizer_free(minimizer);
  gsl_vector_free(zero);
  gsl_vector_free(state.factor);
  for (gsl_vector* feature : state.all_features)
    gsl_vector_free(feature);

  return weights;
}


void IntVectorToFile(const std::vector<int>& data, const char* fname)
{
  FILE* file = fopen(fname, "w");
  if (!file) {
    fprintf (stderr, "Error: Could not open file %s\n", fname);
    exit(-1);
  }

  fprintf(file, "%lu\n", data.size());

  for (size_t i = 0; i < data.size(); ++i)
    fprintf(file, "%d\n", data[i] + 1);

  fclose(file);

  /*
    for (expert) {
    Q[i] = lstdq(expert);
    }*/
}

// from Beliefbox ? (from https://github.com/olethrosdc/beliefbox)
#include "Distribution.h"

std::pair<size_t, size_t> DrunkPlayer(TicTacToe& game, real eps)
{
  size_t move;
  if (MersenneTwister::uniform() > eps)
    move = minmax(game);
  else
    move = MersenneTwister::random() % game.NumberOfMoves();
  return game.MakeMove(move);
}

std::pair<size_t, size_t> OptimalPlayer(TicTacToe& game)
{
  return game.MakeMove(minmax(game));
}

std::pair<size_t, size_t> RandomPlayer(TicTacToe& game)
{
  return DrunkPlayer(game, 1.0);
}


#include "PengKun.h"

std::pair<size_t, size_t> PengKunPlayer(TicTacToe& game, size_t n_descent = 100)
{
  Node root(nullptr, TicTacToe(game), 0);
  root.Visit();
  root.SetGDis(Gaussian(0, 1));

  for (size_t i = 0; i < n_descent; ++i) {
    root.Descent(game.NextPlayer(),
                 [] (TicTacToe& x) { return DrunkPlayer(x, 1.0); },
                 [] (TicTacToe& x) { return DrunkPlayer(x, 1.0); });
  }

  return game.MakeMove(root.BestAction());
}

std::pair<size_t, size_t> FeaturePlayer(TicTacToe& game, const Vector& weights)
{
  std::vector<double> tmp;
  game.ForEachMove([&] (size_t, TicTacToe&& next) {
      tmp.push_back(Product(next.features(), weights));
    });
  Vector tmpV(tmp);
  Vector pi(0);
  SoftMax(tmpV, pi, 1.0);

  return game.MakeMove(DiscreteDistribution::generate(pi));
}


std::pair<size_t, size_t> OpposingPlayer(TicTacToe& game, const std::vector<Vector>& weights, size_t n_descents = 100)
{
  double best_g = 0;
  bool has_g = false;
  size_t choice = 0;

  for (size_t i = 0; i < weights.size(); ++i) {
    Node root(nullptr, TicTacToe(game), 0);
    root.Visit();
    root.SetGDis(Gaussian(0, 1));

    for (size_t d = 0; d < n_descents; ++d) {
        root.Descent(game.NextPlayer(),
                     [] (TicTacToe& x) { return DrunkPlayer(x, 1.0); },
                     [&] (TicTacToe& x) { return FeaturePlayer(x, weights[i]); });
    }

    real g = root.GDis().generate();

    if (!has_g || g < best_g) {
      has_g = true;
      best_g  = g;
      choice = root.BestAction();
    }
  }

  return game.MakeMove(choice);
}

template<typename WhitePlayer, typename BlackPlayer>
std::pair<Expert, Expert> Learn(size_t size, const WhitePlayer& whitePlayer, const BlackPlayer& blackPlayer, size_t n_learn)
{
  Expert white;
  Expert black;

  for (size_t i = 0; i < n_learn; ++i) {
    TicTacToe game(size);

    white.NewEpisode();
    black.NewEpisode();

    while (!game.Ended()) {
      TicTacToe old = game;
      if (old.NextPlayer() == Player::White)
        white.Observe(old, whitePlayer(game));
      else
        black.Observe(old, blackPlayer(game));
    }
  }

  return { white, black };
}

template<typename WhitePlayer, typename BlackPlayer>
void TestAgainst(size_t size, const WhitePlayer& whitePlayer, const BlackPlayer& blackPlayer, size_t n_tests, size_t& win, size_t& loss)
{
  win = 0;
  loss = 0;

  for (size_t i = 0; i < n_tests; ++i) {
    TicTacToe game(size);

    while (!game.Ended()) {
      TicTacToe old = game;
      if (old.NextPlayer() == Player::White)
        whitePlayer(game);
      else
        blackPlayer(game);
    }

    if (game.Winner() == Player::White)
      ++win;
    if (game.Winner() == Player::Black)
      ++loss;
  }
}

int main()
{
  // Initialize the RNG
  MersenneTwister::seed();

  std::vector<size_t> descents = { 20, 50, 80 };

  size_t learnt = 100;
  std::vector<Vector> weights_white;
  std::vector<Vector> weights_black;
  size_t game_size = 4;

  for (size_t descent : descents) {
    auto experts =
      Learn(game_size,
            [&] (TicTacToe& game) { return PengKunPlayer(game, descent); },
            [&] (TicTacToe& game) { return RandomPlayer(game); },
            learnt);
    printf("Done %lu\n", descent);
    weights_white.push_back(FitExpert(experts.first));
    weights_black.push_back(FitExpert(experts.second));
  }

  size_t win, loss;
  size_t n_tests = 1000;
  TestAgainst(game_size,
              [&] (TicTacToe& game) { return OpposingPlayer(game, weights_black, 50); },
              [&] (TicTacToe& game) { return RandomPlayer(game); },
              n_tests, win, loss);
  printf("Opposing vs random: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  for (size_t descent : descents) {
    TestAgainst(game_size,
                [&] (TicTacToe& game) { return PengKunPlayer(game, descent); },
                [&] (TicTacToe& game) { return RandomPlayer(game); },
                n_tests, win, loss);
    printf("Pengkun(%lu) vs random: %lu wins, %lu losses, %lu draws\n", descent, win, loss, n_tests - win - loss);
  }


  return 0;
}

#if 0

int main()
{
  // Initialize the RNG
  MersenneTwister::seed();

  double opposing = 1;
  double first = 0.01;
  double second = 0.8;

  std::vector<std::pair<double, double>> demonstrations = {
    { first, opposing },
    { second, opposing },
  };

  std::vector<Vector> weights;

  size_t learnt = 1000;
  printf("Learning with %lu\n", learnt);
  for (const auto& pair : demonstrations) {
    auto experts =
      Learn(3,
            [&] (TicTacToe& game) { return DrunkPlayer(game, pair.first); },
            [&] (TicTacToe& game) { return DrunkPlayer(game, pair.second); },
            learnt);
    weights.push_back(FitExpert(experts.first));
  }

  size_t win, loss;
  size_t n_tests = 10000;
  TestAgainst(3,
              [&] (TicTacToe& game) { return FeaturePlayer(game, weights[0]); },
              [&] (TicTacToe& game) { return DrunkPlayer(game, opposing); },
              n_tests, win, loss);
  printf("Learn(%.2lf) vs Drunk(%.2lf): %lu wins, %lu losses, %lu draws\n", first, opposing, win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&] (TicTacToe& game) { return FeaturePlayer(game, weights[1]); },
              [&] (TicTacToe& game) { return DrunkPlayer(game, opposing); },
              n_tests, win, loss);
  printf("Learn(%.2lf) vs Drunk(%.2lf): %lu wins, %lu losses, %lu draws\n", second, opposing, win, loss, n_tests - win - loss);

  printf("Aggregate %.2lf / %.2lf\n", (1/first), (1/second));

  Vector combined = (weights[0] * (1/first) + weights[1] * (1/second)) * (1 / (1/second + 1/first));
  TestAgainst(3,
              [&] (TicTacToe& game) { return FeaturePlayer(game, combined); },
              [&] (TicTacToe& game) { return DrunkPlayer(game, opposing); },
              n_tests, win, loss);
  printf("Aggregated vs Drunk(%.2lf): %lu wins, %lu losses, %lu draws\n", opposing, win, loss, n_tests - win - loss);

  double mean = (first + second) / 2;
  TestAgainst(3,
              [&] (TicTacToe& game) { return DrunkPlayer(game, mean); },
              [&] (TicTacToe& game) { return DrunkPlayer(game, opposing); },
              n_tests, win, loss);
  printf("Drunk(%.2lf) vs Drunk(%.2lf): %lu wins, %lu losses, %lu draws\n", mean, opposing, win, loss, n_tests - win - loss);


  return 0;
}
#endif

#if 0
//
int main()
{
  // Initialize the RNG
  MersenneTwister::seed();

  std::vector<std::pair<double, double>> games = {
    { 0.2, 0.0 },
    { 0.1, 0.2 },
    { 0.0, 0.4 },
    { 0.0, 0.0 },
    { 0.4, 0.0 },
  };

  std::vector<Vector> weights_white;
  std::vector<Vector> weights_black;

  for (const auto& pair : games) {
    printf("Playing 100 games of %lf-drunk vs %lf-drunk...\n", pair.first, pair.second);
    auto experts =
      Learn(3,
            [&pair] (TicTacToe& game) { return DrunkPlayer(game, pair.first); },
            [&pair] (TicTacToe& game) { return DrunkPlayer(game, pair.second); },
            100);
    weights_white.push_back(FitExpert(experts.first));
    weights_black.push_back(FitExpert(experts.second));
  }

  size_t win, loss;
  size_t n_tests = 1000;
  TestAgainst(3,
              [&weights_black] (TicTacToe& game) { return OpposingPlayer(game, weights_black); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);

  printf("Opposing vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&weights_black] (TicTacToe& game) { return OpposingPlayer(game, weights_black); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
              n_tests, win, loss);

  printf("Opposing vs random: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [] (TicTacToe& game) { return PengKunPlayer(game); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);

  printf("Pengkun vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [] (TicTacToe& game) { return PengKunPlayer(game); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
              n_tests, win, loss);

  printf("Pengkun vs random: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);


  for (size_t i = 0; i < weights_white.size(); ++i) {
    TestAgainst(3,
                [&weights_white, i] (TicTacToe& game) { return FeaturePlayer(game, weights_white[i]); },
                [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
                n_tests, win, loss);

    printf("Features (%lu) vs optimal: %lu wins, %lu losses, %lu draws\n", i, win, loss, n_tests - win - loss);



    TestAgainst(3,
                [&weights_white, i] (TicTacToe& game) { return FeaturePlayer(game, weights_white[i]); },
                [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
                n_tests, win, loss);

    printf("Features (%lu) vs random: %lu wins, %lu losses, %lu draws\n", i, win, loss, n_tests - win - loss);
  }

  return 0;
}

#endif

#if 0
int main()
{

  /*
  // Against Pengkun Liu's code (https://github.com/Charles-Lau-/thesis/)
  // TODO add the required plumbing to use Pengkun's code

  TicTacToe game(3);
  Board pengkun(3);

  ComputerPlayer pplayer(BLACK);

  char result;

  while (true) {
  auto pair = game.MakeMove(minmax(x));
  pengkun.makeMove(pair.first, pair.second, WHITE);

  if (game.Ended())
  break;

  result = pplayer.algorithmPlay(pengkun);

  if (result != CONTINUE)
  break;
  }
  */ 


  MersenneTwister::seed();

  // opposing

  size_t n_learn = 100;

  printf("Learning on %lu trajectories optimal vs optimal on n=%lu\n", n_learn, 3);

  Expert expert1 =
    Learn(3,
          Player::Black,
          [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
          [] (TicTacToe& game) { return DrunkPlayer(game, 0.3); },
          n_learn);

  Expert expert2 =
    Learn(3,
          Player::Black,
          [] (TicTacToe& game) { return DrunkPlayer(game, 0.4); },
          [] (TicTacToe& game) { return DrunkPlayer(game, 0.2); },
          n_learn);

  std::vector<Vector> weights = { FitExpert(expert1), FitExpert(expert2) };

  size_t win, loss;
  size_t n_tests = 1000;
  TestAgainst(3,
              [&weights] (TicTacToe& game) { return OpposingPlayer(game, weights); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);
  printf("Opposing vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&weights] (TicTacToe& game) { return OpposingPlayer(game, { weights[0] }); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);
  printf("Opposing1 vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&weights] (TicTacToe& game) { return OpposingPlayer(game, { weights[1] }); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);
  printf("Opposing2 vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);


  TestAgainst(3,
              PengKunPlayer,
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);
  printf("Peng kun vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  /*
  TestAgainst(3,
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              [&weights] (TicTacToe& game) { return FeaturePlayer(game, weights[0]); },
              n_tests, win, loss);
  printf("Optimal vs learnt1: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              [&weights] (TicTacToe& game) { return FeaturePlayer(game, weights[1]); },
              n_tests, win, loss);
  printf("Optimal vs learnt2: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);
  */

  /*
  for (size_t i = 0; i < 100; ++i) {
    Vector random_random =
      FitExpert(
                Learn(3, Player::White,
                      [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
                      [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
                      10));

    Vector optimal_random =
      FitExpert(
                Learn(3, Player::White,
                      [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
                      [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
                      10));

    printf("random-random L2 norm: %lf\n", random_random.L2Norm());

    printf("optimal-random L2 norm: %lf\n", optimal_random.L2Norm());

    printf("--\n\n");
    } */

  /*
  size_t n_learn = 10;

  printf("Learning on %lu trajectories random vs random on n=%lu\n", n_learn, 3);
  
  Expert random;
  for (size_t i = 0; i < n_learn; ++i) {
    TicTacToe game(3);

    random.NewEpisode();

    while (!game.Ended()) {
      TicTacToe old = game;
      if (old.NextPlayer() == Player::White)
        random.Observe(old, DrunkPlayer(game, 1.0));
      else
        DrunkPlayer(game, 1.0);
    }
  }

  Vector weights = FitExpert(expert);

  printf("Learning on %lu trajectories random vs random on n=%lu\n", n_learn, 3);
  
  Expert optimal;
  for (size_t i = 0; i < n_learn; ++i) {
    TicTacToe game(3);

    optimal.NewEpisode();

    while (!game.Ended()) {
      TicTacToe old = game;
      if (old.NextPlayer() == Player::White)
        optimal.Observe(old, DrunkPlayer(game, 1.0));
      else
        DrunkPlayer(game, 1.0);
    }
  }

  Vector weights = FitExpert(expert);
  */
  /*

  printf("Weights: ");
  for (int i = 0; i < weights.Size(); ++i)
    printf("%lf ", weights[i]);
  printf("\n");

  size_t n_tests = 100;

  size_t win, loss;
  TestAgainst(3,
              [&weights] (TicTacToe& game) { return FeaturePlayer(game, weights); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
              n_tests, win, loss);
  printf("Learnt vs random: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&weights] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 1.0); },
              n_tests, win, loss);
  printf("Optimal vs random: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  TestAgainst(3,
              [&weights] (TicTacToe& game) { return FeaturePlayer(game, weights); },
              [] (TicTacToe& game) { return DrunkPlayer(game, 0.0); },
              n_tests, win, loss);
  printf("Learnt vs optimal: %lu wins, %lu losses, %lu draws\n", win, loss, n_tests - win - loss);

  printf("--\n");
  printf("\n");*/

  return 0;
}

int old_main()
{


  /*
  // Against Pengkun Liu's code (https://github.com/Charles-Lau-/thesis/)
  // TODO add the required plumbing to use Pengkun's code

  TicTacToe game(3);
  Board pengkun(3);

  ComputerPlayer pplayer(BLACK);

  char result;

  while (true) {
  auto pair = game.MakeMove(minmax(x));
  pengkun.makeMove(pair.first, pair.second, WHITE);

  if (game.Ended())
  break;

  result = pplayer.algorithmPlay(pengkun);

  if (result != CONTINUE)
  break;
  }
  */ 


  MersenneTwister::seed();

  TicTacToe x(3);

  size_t white_win = 0;
  size_t black_win = 0;

  Player me = Player::White;

  for (size_t i = 0; i < 100; ++i) {
    // printf("%lu\n", i);
    x = TicTacToe(3);

    while (!x.Ended()) {
      if (x.NextPlayer() == me)
        PengKunPlayer(x);
      else
        DrunkPlayer(x, 0.0);
    }

    if (x.Winner() == Player::White)
      white_win++;
    if (x.Winner() == Player::Black)
      black_win++;
  }

  printf("%lu wins, %lu losses, %lu draws\n", white_win, black_win, 100 - white_win - black_win);

  return 0;

  x = TicTacToe(3);

  Expert drunk(false);

  // drunk vs drunk

  /*
  printf("Learning on %lu runs\n", 100);
  for (size_t i = 0; i < 100; ++i) {
    drunk.NewEpisode();
    x = TicTacToe(3);
    while (!x.Ended()) {
      TicTacToe old = x;
      if (old.NextPlayer() == me) {
        size_t move;
        if (MersenneTwister::uniform() > 0.2)
          move = minmax(x);
        else
          move = MersenneTwister::random() % x.NumberOfMoves();
        auto pair = x.MakeMove(move);

        assert (!old.Ended());
        drunk.Observe(old, pair);
      } else {
        size_t move;
        if (MersenneTwister::uniform() > 0)
          move = minmax(x);
        else
          move = MersenneTwister::random() % x.NumberOfMoves();
        auto pair = x.MakeMove(move);
      }
    }
  }
  */

  Expert optimal(false);

  // optimal vs drunk
  printf("Learning optimal vs optimal %lu\n", 100);
  for (size_t i = 0; i < 10; ++i) {
    optimal.NewEpisode();
    x = TicTacToe(3);
    while (!x.Ended()) {
      TicTacToe old = x;
      if (old.NextPlayer() == me)
        optimal.Observe(old, OptimalPlayer(x));
      else
        optimal.Observe(old, OptimalPlayer(x));
    }
  }

  printf("fit optimal\n");

  Vector optimal_weight = FitExpert(optimal);

  /*
  printf("fit drunk\n");
  Vector drunk_weight = FitExpert(drunk);
  */

  printf("\n\nOptimal weights: ");
    for (int i = 0; i < optimal_weight.Size(); ++i)
    printf("%lf ", optimal_weight[i]);
  printf("\n");

  /*
  printf("\n\nDrunk weights: ");
  for (int i = 0; i < drunk_weight.Size(); ++i)
    printf("%lf ", drunk_weight[i]);
  printf("\n");
  */

  // How to play with the weight??

  size_t wins = 0;
  size_t loss = 0;
  size_t nb_runs = 100000;

  printf("Playing %lu vs drunk(1.0) now!\n", nb_runs);

  auto weight = optimal_weight;

  for (size_t i = 0; i < nb_runs; ++i) {
    x = TicTacToe(3);

    while (!x.Ended()) {
      if (x.NextPlayer() == me) {
        std::vector<double> tmp;
        x.ForEachMove([&] (size_t, TicTacToe&& next) {
            tmp.push_back(Product(next.features(), weight));
          });
        Vector tmpV(tmp);
        Vector pi(0);
        SoftMax(tmpV, pi, 1.0);

        int i = DiscreteDistribution::generate(pi);

        x.MakeMove(i);
        // printf("After learnt move for X (%lu, %lu): \n", move.first, move.second);
      } else
        DrunkPlayer(x, 1.0);
    }

    if (x.Winner() == me)
      wins++;
    else if (x.Winner() != Player::Blank)
      loss++;
  }

  printf("%lu wins, %lu loss, %lu draws\n", wins, loss, nb_runs - wins - loss);

  /*
    x.print(stdout);
    x.MakeMove(minmax(x));
    x.print(stdout);
    printf("Fixed for player %c\n", x.NextPlayer() == Player::White ? 'X' : 'O');
    x.MakeMove(minmax(x));
    printf("\n");
    x.print(stdout);

    auto feat = x.features();

    printf("Features: \n");
    for (int i = 0; i < feat.Size(); ++i)
    printf("%lf ", feat[i]);
    printf("\n\n");

    x.MakeMove(minmax(x));
    x.print(stdout);

    feat = x.features();
    printf("Features: \n");
    for (int i = 0; i < feat.Size(); ++i)
    printf("%lf ", feat[i]);
    printf("\n\n");

    std::vector<int> data { };
    FileToIntVector(data, "test.dat", 0);

    for (size_t i = 0; i < data.size(); ++i)
    printf("%d ", data[i]);

    IntVectorToFile(data, "out.dat");
  */

  return 0;
}
#endif 
