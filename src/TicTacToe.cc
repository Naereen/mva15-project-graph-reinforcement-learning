// (C) Basile Clement and Lilian Besson, 2015
// https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
#include "TicTacToe.h"

#include <cstdio>

void TicTacToe::Play(size_t i, size_t j)
{
  if (m_ended)
    throw std::runtime_error("Trying to play after the game is over");
  if (m_board(i, j) != Player::Blank)
    throw std::runtime_error("Trying to play on a non-empty position");
  m_board(i, j) = m_nextPlayer;

  // Check the row
  if (!m_ended) {
    size_t n = 0;
    for (size_t jj = 0; jj < Size(); ++jj)
      if (m_board(i, jj) == m_nextPlayer) ++n;
    m_ended |= n == Size();
  }
  // Check the column
  if (!m_ended) {
    size_t n = 0;
    for (size_t ii = 0; ii < Size(); ++ii)
      if (m_board(ii, j) == m_nextPlayer) ++n;
    m_ended |= n == Size();
  }
  // Check the diagonals
  if (!m_ended && i == j) {
    size_t n = 0;
    for (size_t ii = 0; ii < Size(); ++ii)
      if (m_board(ii, ii) == m_nextPlayer) ++n;
    m_ended |= n == Size();
  }

  if (!m_ended && i + j == Size() - 1) {
    size_t n = 0;
    for (size_t ii = 0; ii < Size(); ++ii)
      if (m_board(ii, Size() - 1 - ii) == m_nextPlayer) ++n;
    m_ended |= n == Size();
  }

  if (!m_ended)
    m_nextPlayer = m_nextPlayer == Player::White ? Player::Black : Player::White;
}

void TicTacToe::print(FILE* f) const
{
  for (size_t i = 0; i < Size(); ++i) {
    for (size_t j = 0; j < Size(); ++j) {
      switch (m_board(i, j)) {
      case Player::Blank: fprintf(f, " _"); break;
      case Player::White: fprintf(f, " X"); break;
      case Player::Black: fprintf(f, " O"); break;
      }
    }
    fprintf(f, "\n");
  }
}

Vector TicTacToe::features() const
{
  // We use the following features (we *don't* count size 0)
  // - uplets of size n for X/O are lines, columns or diagonals where X/O has exactly n marks and the rest are empty
  //   [ size 2n-2 ]
  // - the number of points on the diagonals for X/O
  //   [ size 2 ]
  // - who has the center
  //   [ size 1]
  // - n-diversity, the number of directions for uplets of size n (each diagonal is a separate direction)
  //   [ size 2n-2 ]
  // - TODO: We should add crosspoints

  std::vector<size_t> n_white(Size() - 1);
  std::vector<size_t> n_black(Size() - 1);

  std::vector<bool> white_col(Size() - 1);
  std::vector<bool> white_line(Size() - 1);
  std::vector<bool> black_col(Size() - 1);
  std::vector<bool> black_line(Size() - 1);

  size_t n_white_diag = 0;
  size_t n_black_diag = 0;
  size_t n_white_antidiag = 0;
  size_t n_black_antidiag = 0;

  for (size_t i = 0; i < Size(); ++i) {
    size_t n_white_line = 0, n_white_col = 0;
    size_t n_black_line = 0, n_black_col = 0;
    for (size_t j = 0; j < Size(); ++j) {
      switch (m_board(i, j)) {
      case Player::Blank: break;
      case Player::White: ++n_white_line; break;
      case Player::Black: ++n_black_line; break;
      }
      switch (m_board(j, i)) {
      case Player::Blank: break;
      case Player::White: ++n_white_col; break;
      case Player::Black: ++n_black_col; break;
      }
    }
    if (n_black_line == 0 && n_white_line > 0 && n_white_line < Size()) {
      ++n_white[n_white_line - 1];
      white_line[n_white_line - 1] = true;
    }
    if (n_black_col == 0 && n_white_col > 0 && n_white_col < Size()) {
      ++n_white[n_white_col - 1];
      white_col[n_white_col - 1] = true;
    }
    if (n_white_line == 0 && n_black_line > 0 && n_black_line < Size()) {
      ++n_black[n_black_line - 1];
      black_line[n_black_line - 1] = true;
    }
    if (n_white_col == 0 && n_black_col > 0 && n_black_col < Size()) {
      ++n_black[n_black_col - 1];
      black_col[n_black_col - 1] = true;
    }

    switch (m_board(i, i)) {
    case Player::Blank: break;
    case Player::White: n_white_diag++; break;
    case Player::Black: n_black_diag++; break;
    }

    switch (m_board(i, Size() - i - 1)) {
    case Player::Blank: break;
    case Player::White: n_white_antidiag++; break;
    case Player::Black: n_black_antidiag++; break;
    }
  }

  std::vector<size_t> white_diversity(Size() - 1);
  std::vector<size_t> black_diversity(Size() - 1);

  if (n_white_diag == 0 && n_black_diag > 0 && n_black_diag < Size()) {
    ++n_black[n_black_diag - 1];
    ++black_diversity[n_black_diag - 1];
  }

  if (n_black_diag == 0 && n_white_diag > 0 && n_white_diag < Size()) {
    ++n_white[n_white_diag - 1];
    ++white_diversity[n_white_diag - 1];
  }

  if (n_white_antidiag == 0 && n_black_antidiag > 0 && n_black_antidiag < Size()) {
    ++n_black[n_black_antidiag - 1];
    ++black_diversity[n_black_antidiag - 1];
  }

  if (n_black_antidiag == 0 && n_white_antidiag > 0 && n_white_antidiag < Size()) {
    ++n_white[n_white_antidiag - 1];
    ++white_diversity[n_white_antidiag - 1];
  }

  for (size_t i = 0; i < Size() - 1; ++i) {
    white_diversity[i] = white_col[i] + white_line[i];
    black_diversity[i] = black_col[i] + black_line[i];
  }

  Vector features { Vector::Null(4 * Size() - 2) };
  size_t pos = 0;

  for (size_t i = 0; i < Size() - 1; ++i) {
    features[pos++] = n_white[i];
    features[pos++] = n_black[i];
  }

  features[pos++] = n_white_diag + n_white_antidiag;
  features[pos++] = n_black_diag + n_black_antidiag;

  /*
  if (Size() % 2 == 1) {
    switch (m_board(Size() / 2, Size() / 2)) {
    case Player::Blank: features[pos++] =0; break;
    case Player::White: features[pos++] = 1; break;
    case Player::Black: features[pos++] = -1; break;
    }
    }*/

  for (size_t i = 0; i < Size() - 1; ++i) {
    features[pos++] = white_diversity[i];
    features[pos++] = black_diversity[i];
  }

  assert(int(pos) == features.Size());

  size_t n_one_dim = pos;

  features.Resize( (n_one_dim * (n_one_dim + 1)) / 2);

  for (size_t i = 0; i < n_one_dim; ++i) {
    for (size_t j = i + 1; j < n_one_dim; ++j) {
      features[pos++] = features[i] * features[j];
    }
  }

  return features;
}
